CC=gcc
CFLAGS=-I -Wall

all: client server

client: client.o helper.o
	$(CC) -o client client.o helper.o

server: server.o helper.o
	$(CC) -o server server.o helper.o

.PHONY : clean
clean :
	-rm *.o $(objects) client server
